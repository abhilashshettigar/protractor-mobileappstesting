"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tsNode = require("ts-node");
var serverAddress = 'http://localhost:4723/wd/hub';
var testFilePAtterns = [
    '**/*/*.e2e-spec.ts'
];
var iPhoneXCapability = {
    browserName: '',
    autoWebview: true,
    autoWebviewTimeout: 20000,
    app: '',
    version: '11.4',
    platform: 'iOS',
    deviceName: 'iPhone X',
    platformName: 'iOS',
    name: 'My First Mobile Test',
    automationName: 'XCUITest',
    nativeWebTap: 'true'
};
var androidOneplusOneCapability = {
    browserName: '',
    autoWebview: true,
    autoWebviewTimeout: 20000,
    platformName: 'Android',
    platformVersion: '6.0.1',
    deviceName: '580c03fc',
    app: './e2e/apps/app-debug.apk',
    appPackage: 'com.rentalq.app',
    appActivity: 'MainActivity',
    autoAcceptAlerts: 'true',
    autoGrantPermissions: 'true',
    newCommandTimeout: 300000
};
exports.config = {
    allScriptsTimeout: 11000,
    specs: testFilePAtterns,
    multiCapabilities: [
        androidOneplusOneCapability,
    ],
    framework: 'jasmine',
    SELENIUM_PROMISE_MANAGER: false,
    useAllAngular2AppRoots: true,
    jasmineNodeOpts: {
        showColors: true,
        defaultTimeoutInterval: 30000
    },
    seleniumAddress: serverAddress,
    onPrepare: function () {
        tsNode.register({
            project: 'e2e/tsconfig.json'
        });
    }
    // beforeLaunch: async () => {
    //   require('ts-node').register({
    //     project: 'e2e'
    //   });
    // }
};
//# sourceMappingURL=protractor.config.js.map