import {Config} from 'protractor';
import * as tsNode from 'ts-node';

const serverAddress = 'http://localhost:4723/wd/hub';
const testFilePAtterns: Array<string> = [
  '**/*/*.e2e-spec.ts'
];
const iPhoneXCapability = {
  browserName: '',
  autoWebview: true,
  autoWebviewTimeout: 20000,
  app: '',
  version: '11.4',
  platform: 'iOS',
  deviceName: 'iPhone X',
  platformName: 'iOS',
  name: 'My First Mobile Test',
  automationName: 'XCUITest',
  nativeWebTap: 'true'
};
const androidOneplusOneCapability = {
  browserName: '',
  autoWebview: true,
  autoWebviewTimeout: 20000,
  platformName: 'Android',
  platformVersion:'6.0.1',
  deviceName: '580c03fc',
  app: './e2e/apps/app-debug.apk',
  appPackage: 'com.rentalq.app',
  appActivity: 'MainActivity',
  autoAcceptAlerts: 'true',
  autoGrantPermissions: 'true',
  newCommandTimeout: 300000
};

export let config: Config = {
  allScriptsTimeout: 11000,
  specs: testFilePAtterns,
  multiCapabilities: [
    androidOneplusOneCapability,
    ],
 
  framework: 'jasmine',
  SELENIUM_PROMISE_MANAGER: false,
  useAllAngular2AppRoots: true,
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000
  },
  
  seleniumAddress: serverAddress,
  onPrepare: () => {
    tsNode.register({
      project: 'e2e/tsconfig.json'
    });
  }
  // beforeLaunch: async () => {
  //   require('ts-node').register({
  //     project: 'e2e'
  //   });
  // }
};